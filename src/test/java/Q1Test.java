import org.junit.Test;
import static org.junit.Assert.*;

public class Q1Test {

    @Test public void acceptStringsOf1001OrMore() {
        Q1 classUnderTest = new Q1();
        assertEquals("Sample1 is incorrect", 3, classUnderTest.getMaxSizeOfMatchingPattern("abcdefghij", "def"));
    }

    @Test public void testSample2ReturnCorrectValue() {
        Q1 classUnderTest = new Q1();
        assertEquals("Sample2 is incorrect", -1, classUnderTest.getMaxSizeOfMatchingPattern("thisisaplaintext", "a*bc"));
    }

    @Test public void testSample3ReturnCorrectValue() {
        Q1 classUnderTest = new Q1();
        assertEquals("Sample3 is incorrect", 19, classUnderTest.getMaxSizeOfMatchingPattern("recruitcommunications", "r.*c.*o"));
    }

    @Test public void testSample4ReturnCorrectValue() {
        Q1 classUnderTest = new Q1();
        assertEquals("Sample4 is incorrect", 3, classUnderTest.getMaxSizeOfMatchingPattern("aaabbbqqqzzz", "q*"));
    }

    @Test public void testSample5ReturnCorrectValue() {
        Q1 classUnderTest = new Q1();
        assertEquals("Sample5 is incorrect", 8, classUnderTest.getMaxSizeOfMatchingPattern("lxyxyzrcolxyyyyrco", "xy*z?rco"));
    }
}
