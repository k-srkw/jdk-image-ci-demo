import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;

public class Q1 {

    public int getMaxSizeOfMatchingPattern(String targetText, String regex) {
        OrgPattern orgp = OrgPattern.compile(regex);

        Set<String> results = orgp.find(targetText);

        int maxSizeOfGroup = -1;

        for(String result : results) {
            int sizeOfGroup = result.length();
            if(sizeOfGroup > maxSizeOfGroup){
                maxSizeOfGroup = sizeOfGroup;
            }
        }

        return maxSizeOfGroup;
    }

    public static void main(String[] args) {
        String targetText = null;
        String regex = null;

        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        try {
            targetText = new String(input.readLine());
            if(targetText.length() > 1000) {
                System.out.println("Input text is invalid.");
                return;
            }
            regex = new String(input.readLine());
            if(regex.length() > 1000) {
                System.out.println("Input regex is invalid.");
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(new Q1().getMaxSizeOfMatchingPattern( targetText, regex ));
    }

}