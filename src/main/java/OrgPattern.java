import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OrgPattern {

    private String regex;
    private List<Operation> byteCodes;

    private OrgPattern(String regex, List<Operation> byteCodes) {
        this.regex = regex;
        this.byteCodes = byteCodes;
    }

    public Set<String> find(String targetText) {
        CopyOnWriteArraySet<String> results = new CopyOnWriteArraySet<>();

        if(!regex.isEmpty()) {
            ExecutorService executorService = Executors.newSingleThreadExecutor();
                for(int i = 0; i < targetText.length(); i++) {
                    if(targetText.charAt(i) == regex.charAt(0)) {
                    executorService.execute(new OrgMatcher(byteCodes, targetText.substring(i), results));
                }
            }

            executorService.shutdown();
            while(!executorService.isTerminated());
        }

        return results;
    }

    public static OrgPattern compile(String regex) {

        List<Operation> byteCodes = new ArrayList<>();
        int regexIndex = 0;

        if(!regex.isEmpty()) {
            do {
                if(regexIndex + 1 < regex.length()) {
                    if(regex.charAt(regexIndex + 1) == '?') {
                        byteCodes.add(new Operation(Operation.OpCode.Split, byteCodes.size() + 1 , byteCodes.size() + 2));
                        byteCodes.add(new Operation(Operation.OpCode.Char, regex.charAt(regexIndex)));
                        regexIndex += 2;
                        continue;
                    } else if(regex.charAt(regexIndex + 1) == '*') {
                        byteCodes.add(new Operation(Operation.OpCode.Split, byteCodes.size() + 1, byteCodes.size() + 3));
                        byteCodes.add(new Operation(Operation.OpCode.Char, regex.charAt(regexIndex)));
                        byteCodes.add(new Operation(Operation.OpCode.Jmp, byteCodes.size() - 2));
                        regexIndex += 2;
                        continue;
                    }
                }

                byteCodes.add(new Operation(Operation.OpCode.Char, regex.charAt(regexIndex)));
                regexIndex++;

            } while( regexIndex < regex.length());
        }

        byteCodes.add(new Operation(Operation.OpCode.Match));

        return new OrgPattern(regex, byteCodes);
    }
}