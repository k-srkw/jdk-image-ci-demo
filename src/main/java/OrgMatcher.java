import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OrgMatcher implements Runnable {

    private int programCounter;
    private int stringPointer;
    private StringBuilder group;
    private  List<Operation> byteCodes;
    private String targetText;
    private CopyOnWriteArraySet<String> results;

    public OrgMatcher(){
        this.programCounter = 0;
        this.stringPointer = 0;
        this.group = new StringBuilder();
    }

    public OrgMatcher(List<Operation> byteCodes, String targetText, CopyOnWriteArraySet<String> results) {
        this();
        this.byteCodes = byteCodes;
        this.targetText = targetText;
        this.results = results;
    }

    public OrgMatcher(int programCounter, int stringPointer, StringBuilder group, List<Operation> byteCodes, String targetText, CopyOnWriteArraySet<String> results) {
        this.programCounter = programCounter;
        this.stringPointer = stringPointer;
        this.group = group;
        this.byteCodes = byteCodes;
        this.targetText = targetText;
        this.results = results;
    }

    @Override
    public void run() {
        while(true) {
            switch (byteCodes.get(programCounter).opCode) {
                case Char:
                    if(stringPointer >= targetText.length()) {
                        break;
                    } else if(byteCodes.get(programCounter).letter == targetText.charAt(stringPointer) || byteCodes.get(programCounter).letter == '.'){
                        group.append(targetText.charAt(stringPointer));
                        programCounter++;
                        stringPointer++;
                        continue;
                    }
                    break;
                case Match:
                    results.add(group.toString());
                    break;
                case Jmp:
                    programCounter = byteCodes.get(programCounter).indexT1;
                    continue;
                case Split:
                    ExecutorService executorService = Executors.newSingleThreadExecutor();

                    executorService.execute(new OrgMatcher(byteCodes.get(programCounter).indexT1, stringPointer, new StringBuilder(group.toString()), byteCodes, targetText, results));
                    executorService.execute(new OrgMatcher(byteCodes.get(programCounter).indexT2, stringPointer, new StringBuilder(group.toString()), byteCodes, targetText, results));

                    executorService.shutdown();
                    while(!executorService.isTerminated());

                    break;
            }

            break;
        }
    }
}