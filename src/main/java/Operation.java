public class Operation {

    protected OpCode opCode;
    protected char letter;
    protected int indexT1;
    protected int indexT2;

    public enum OpCode {
        Char, Match, Jmp, Split
    }

    public Operation(OpCode opCode) {
        this.opCode = opCode;
    }

    public Operation(OpCode opCode, char letter) {
        this(opCode);
        this.letter = letter;
    }

    public Operation(OpCode opCode, int indexT1) {
        this(opCode);
        this.indexT1 = indexT1;
    }

    public Operation(OpCode opCode, int indexT1, int indexT2) {
        this(opCode, indexT1);
        this.indexT2 = indexT2;
    }
}